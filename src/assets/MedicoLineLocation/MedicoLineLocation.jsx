/*
We're constantly improving the code you see. 
Please share your feedback here: https://form.asana.com/?k=uvp-HPgd3_hyoXRBw1IcNg&d=1152665201300829
*/

import React from "react";

export const MedicoLineLocation = ({ className }) => {
  return (
    <svg
      className={`medico-line-location ${className}`}
      fill="none"
      height="35"
      viewBox="0 0 35 35"
      width="35"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        className="path"
        d="M6.56251 12.3958C9.22795 1.02675 25.7856 1.02674 28.4375 12.3958C29.916 18.7329 21.875 31.3542 17.5 31.3542C13.125 31.3542 5.08279 18.7382 6.56251 12.3958Z"
        stroke="black"
      />
      <path
        className="path"
        d="M24.0625 14.5833C24.0625 18.2077 21.1244 21.1458 17.5 21.1458C13.8756 21.1458 10.9375 18.2077 10.9375 14.5833C10.9375 10.9589 13.8756 8.02081 17.5 8.02081C21.1244 8.02081 24.0625 10.9589 24.0625 14.5833Z"
        stroke="black"
      />
    </svg>
  );
};
