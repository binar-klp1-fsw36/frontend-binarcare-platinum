import "./Column.css";
import { useState } from "react";
import Card from "../Card/Card";

const test = [
  {
    alt: "gambar test",
    src: "https://images.unsplash.com/photo-1576091160550-2173dba999ef?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80",
    text: "Lorem ipsum",
    redirect: "/read",
  },
  {
    alt: "gambar test",
    src: "https://images.unsplash.com/photo-1576091160550-2173dba999ef?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80",
    text: "Lorem ipsum",
    redirect: "/read",
  },
  {
    alt: "gambar test",
    src: "https://images.unsplash.com/photo-1576091160550-2173dba999ef?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80",
    text: "Lorem ipsum",
    redirect: "/read",
  },
  {
    alt: "gambar test",
    src: "https://images.unsplash.com/photo-1576091160550-2173dba999ef?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80",
    text: "Lorem ipsum",
    redirect: "/read",
  },
  {
    alt: "gambar test",
    src: "https://images.unsplash.com/photo-1576091160550-2173dba999ef?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80",
    text: "Lorem ipsum",
    redirect: "/read",
  },
  {
    alt: "gambar test",
    src: "https://images.unsplash.com/photo-1576091160550-2173dba999ef?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80",
    text: "Lorem ipsum",
    redirect: "/read",
  },
  {
    alt: "gambar test",
    src: "https://images.unsplash.com/photo-1576091160550-2173dba999ef?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80",
    text: "Lorem ipsum",
    redirect: "/read",
  },
  {
    alt: "gambar test",
    src: "https://images.unsplash.com/photo-1576091160550-2173dba999ef?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80",
    text: "Lorem ipsum",
    redirect: "/read",
  },
  {
    alt: "gambar test",
    src: "https://images.unsplash.com/photo-1576091160550-2173dba999ef?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80",
    text: "Lorem ipsum",
    redirect: "/read",
  },
  {
    alt: "gambar test",
    src: "https://images.unsplash.com/photo-1576091160550-2173dba999ef?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80",
    text: "Lorem ipsum",
    redirect: "/read",
  },
  {
    alt: "gambar test",
    src: "https://images.unsplash.com/photo-1576091160550-2173dba999ef?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80",
    text: "Lorem ipsum",
    redirect: "/read",
  },
];

function Column() {
  const [amount, setAmount] = useState(5);
  let articleArray = [];

  for (let i = 0; i < amount; i++) {
    articleArray.push(test[i]);
  }

  return (
    <div className="content-container">
      <div className="article-card-container">
        {articleArray.map((item) => {
          return (
            <Card
              alt={item.alt}
              src={item.src}
              text={item.text}
              redirect={item.redirect}
            />
          );
        })}
      </div>
      <div className="button-container">
        <button
          className="show-button"
          onClick={() => {
            if (articleArray.length === test.length)
              return alert("Maksimal artikel sudah ditampilkan");

            if (articleArray.length + 5 <= test.length) setAmount(amount + 5);
            else if (test.length - articleArray.length < 5)
              setAmount(amount + (test.length - articleArray.length));
          }}
        >
          Show More
        </button>
      </div>
    </div>
  );
}

export default Column;
