import ImgBestDoctor from '../assets/best-doctors.svg';

function BestDoctor() {
  return (
    <>
      <div className="bg-white pt-12 pb-10">
        <div className="container mx-auto px-4 max-w-screen-xl">
          <p className="mb-3 font-bold text-2xl text-blue-950">Best Doctors</p>
          <p className="text-base text-gray-500">Help your life much better</p>

          <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-5 gap-8 bg-white mt-10">
            <div class="w-56 h-fit group">
              <div class="relative overflow-hidden">
                <img class="h-72 w-full rounded-3xl object-cover" src={ImgBestDoctor} alt="bestDoctorImage" />

                <div class="absolute h-full w-full rounded-3xl bg-blue-500/70 flex items-center justify-center -bottom-10 group-hover:bottom-0 opacity-0 group-hover:opacity-100 transition-all duration-300">
                  <button class="bg-white rounded-full font-medium text-lg text-blue-600 py-2 px-10">Booking</button>
                </div>
              </div>
              <h2 class="mt-3 text-lg font-semibold text-blue-950 capitalize">Dr. Shin Sage, SpKK(K)</h2>
              <div class="flex items-center justify-between mt-3">
                <p class="text-base text-gray-500">Dermatology</p>
                <div className="flex items-center content-end">
                  <svg class="w-5 h-5 text-yellow-300 mr-1" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 22 20">
                    <path d="M20.924 7.625a1.523 1.523 0 0 0-1.238-1.044l-5.051-.734-2.259-4.577a1.534 1.534 0 0 0-2.752 0L7.365 5.847l-5.051.734A1.535 1.535 0 0 0 1.463 9.2l3.656 3.563-.863 5.031a1.532 1.532 0 0 0 2.226 1.616L11 17.033l4.518 2.375a1.534 1.534 0 0 0 2.226-1.617l-.863-5.03L20.537 9.2a1.523 1.523 0 0 0 .387-1.575Z" />
                  </svg>
                  <p class="text-base font-medium text-blue-950 ">4.90</p>
                </div>
              </div>
            </div>

            <div class="w-56 h-fit group">
              <div class="relative overflow-hidden">
                <img class="h-72 w-full rounded-3xl object-cover" src={ImgBestDoctor} alt="bestDoctorImage" />

                <div class="absolute h-full w-full rounded-3xl bg-blue-500/70 flex items-center justify-center -bottom-10 group-hover:bottom-0 opacity-0 group-hover:opacity-100 transition-all duration-300">
                  <button class="bg-white rounded-full font-medium text-lg text-blue-600 py-2 px-10">Booking</button>
                </div>
              </div>
              <h2 class="mt-3 text-lg font-semibold text-blue-950 capitalize">Dr. Shin Sage, SpKK(K)</h2>
              <div class="flex items-center justify-between mt-3">
                <p class="text-base text-gray-500">Dermatology</p>
                <div className="flex items-center content-center ">
                  <svg class="w-5 h-5 text-yellow-300 mr-1" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 22 20">
                    <path d="M20.924 7.625a1.523 1.523 0 0 0-1.238-1.044l-5.051-.734-2.259-4.577a1.534 1.534 0 0 0-2.752 0L7.365 5.847l-5.051.734A1.535 1.535 0 0 0 1.463 9.2l3.656 3.563-.863 5.031a1.532 1.532 0 0 0 2.226 1.616L11 17.033l4.518 2.375a1.534 1.534 0 0 0 2.226-1.617l-.863-5.03L20.537 9.2a1.523 1.523 0 0 0 .387-1.575Z" />
                  </svg>
                  <p class="text-base font-medium text-blue-950 ">4.90</p>
                </div>
              </div>
            </div>

            <div class="w-56 h-fit group">
              <div class="relative overflow-hidden">
                <img class="h-72 w-full rounded-3xl object-cover" src={ImgBestDoctor} alt="bestDoctorImage" />

                <div class="absolute h-full w-full rounded-3xl bg-blue-500/70 flex items-center justify-center -bottom-10 group-hover:bottom-0 opacity-0 group-hover:opacity-100 transition-all duration-300">
                  <button class="bg-white rounded-full font-medium text-lg text-blue-600 py-2 px-10">Booking</button>
                </div>
              </div>
              <h2 class="mt-3 text-lg font-semibold text-blue-950 capitalize">Dr. Shin Sage, SpKK(K)</h2>
              <div class="flex items-center justify-between mt-3">
                <p class="text-base text-gray-500">Dermatology</p>
                <div className="flex items-center content-center ">
                  <svg class="w-5 h-5 text-yellow-300 mr-1" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 22 20">
                    <path d="M20.924 7.625a1.523 1.523 0 0 0-1.238-1.044l-5.051-.734-2.259-4.577a1.534 1.534 0 0 0-2.752 0L7.365 5.847l-5.051.734A1.535 1.535 0 0 0 1.463 9.2l3.656 3.563-.863 5.031a1.532 1.532 0 0 0 2.226 1.616L11 17.033l4.518 2.375a1.534 1.534 0 0 0 2.226-1.617l-.863-5.03L20.537 9.2a1.523 1.523 0 0 0 .387-1.575Z" />
                  </svg>
                  <p class="text-base font-medium text-blue-950 ">4.90</p>
                </div>
              </div>
            </div>

            <div class="w-56 h-fit group">
              <div class="relative overflow-hidden">
                <img class="h-72 w-full rounded-3xl object-cover" src={ImgBestDoctor} alt="bestDoctorImage" />

                <div class="absolute h-full w-full rounded-3xl bg-blue-500/70 flex items-center justify-center -bottom-10 group-hover:bottom-0 opacity-0 group-hover:opacity-100 transition-all duration-300">
                  <button class="bg-white rounded-full font-medium text-lg text-blue-600 py-2 px-10">Booking</button>
                </div>
              </div>
              <h2 class="mt-3 text-lg font-semibold text-blue-950 capitalize">Dr. Shin Sage, SpKK(K)</h2>
              <div class="flex items-center justify-between mt-3">
                <p class="text-base text-gray-500">Dermatology</p>
                <div className="flex items-center content-center ">
                  <svg class="w-5 h-5 text-yellow-300 mr-1" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 22 20">
                    <path d="M20.924 7.625a1.523 1.523 0 0 0-1.238-1.044l-5.051-.734-2.259-4.577a1.534 1.534 0 0 0-2.752 0L7.365 5.847l-5.051.734A1.535 1.535 0 0 0 1.463 9.2l3.656 3.563-.863 5.031a1.532 1.532 0 0 0 2.226 1.616L11 17.033l4.518 2.375a1.534 1.534 0 0 0 2.226-1.617l-.863-5.03L20.537 9.2a1.523 1.523 0 0 0 .387-1.575Z" />
                  </svg>
                  <p class="text-base font-medium text-blue-950 ">4.90</p>
                </div>
              </div>
            </div>

            <div class="w-56 h-fit group">
              <div class="relative overflow-hidden">
                <img class="h-72 w-full rounded-3xl object-cover" src={ImgBestDoctor} alt="bestDoctorImage" />

                <div class="absolute h-full w-full rounded-3xl bg-blue-500/70 flex items-center justify-center -bottom-10 group-hover:bottom-0 opacity-0 group-hover:opacity-100 transition-all duration-300">
                  <button class="bg-white rounded-full font-medium text-lg text-blue-600 py-2 px-10">Booking</button>
                </div>
              </div>
              <h2 class="mt-3 text-lg font-semibold text-blue-950 capitalize">Dr. Shin Sage, SpKK(K)</h2>
              <div class="flex items-center justify-between mt-3">
                <p class="text-base text-gray-500">Dermatology</p>
                <div className="flex items-center content-center ">
                  <svg class="w-5 h-5 text-yellow-300 mr-1" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 22 20">
                    <path d="M20.924 7.625a1.523 1.523 0 0 0-1.238-1.044l-5.051-.734-2.259-4.577a1.534 1.534 0 0 0-2.752 0L7.365 5.847l-5.051.734A1.535 1.535 0 0 0 1.463 9.2l3.656 3.563-.863 5.031a1.532 1.532 0 0 0 2.226 1.616L11 17.033l4.518 2.375a1.534 1.534 0 0 0 2.226-1.617l-.863-5.03L20.537 9.2a1.523 1.523 0 0 0 .387-1.575Z" />
                  </svg>
                  <p class="text-base font-medium text-blue-950 ">4.90</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default BestDoctor;
