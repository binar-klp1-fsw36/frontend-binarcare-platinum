import React, { useState } from 'react';
import User from '../../pictures/user.png';
import axios from 'axios';

function SelectedUser() {
  const [profilePicture, setProfilePicture] = useState(User);
  const [file, setFile] = useState(null);

  const handleProfilePictureChange = (e) => {
    const selectedFile = e.target.files[0];

    if (selectedFile) {
      setFile(selectedFile);
      setProfilePicture(URL.createObjectURL(selectedFile));
    }
  };

  const handleUploadProfilePicture = async () => {
    if (!file) {
      alert('Please select a new profile picture before uploading.');
      return;
    }

    const formData = new FormData();
    formData.append('avatar', file);

    try {
      const response = await axios.post('http://localhost:4000/api/v1/avatar', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });

      const url = response.data.data.url;
      alert(`Profile picture uploaded successfully. URL: ${url}`);
    } catch (error) {
      console.error('Error:', error);
      alert('Error uploading profile picture.');
    }
  };

  return (
    <>
      <div className="flex items-center sm:space-x-10">
        <div className="bg-white space-y-2 sm:py-4 sm:flex sm:items-center sm:space-y-0 sm:space-x-6">
          <img
            className="block mx-auto h-24 rounded-full sm:mx-0 sm:shrink-0"
            src={profilePicture}
            alt="Profile Picture"
          />
          <div className="text-center space-y-2 sm:text-left">
            <div className="space-y-0.5">
              <p className="text-lg text-black font-semibold">John Doe</p>
              <p className="text-slate-500 font-medium">Patient</p>
              <label className="cursor-pointer">
                <span className="text-blue-600">
                  Change Profile Picture
                </span>
                <input
                  type="file"
                  name="avatar"
                  accept="image/*"
                  style={{ display: 'none' }}
                  onChange={handleProfilePictureChange}
                />
                <button className="text-blue-600 ml-2" onClick={handleUploadProfilePicture}>Upload</button>
              </label>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default SelectedUser;
