import React, { useState, useEffect } from 'react';
import axios from '../../config/axios';

function FetchData() {
  const [data, setData] = useState(null);

  useEffect(() => {
    // Make a GET request when the component mounts
    axios.get('/api/v1/user/1') // Replace with your actual API route
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
      });
  }, []);

  return (
    <div>
      <h2>Sample Data:</h2>
      {data ? (
        <pre>{JSON.stringify(data, null, 2)}</pre>
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
}

export default FetchData;
