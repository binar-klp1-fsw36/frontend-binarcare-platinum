
function UserInfo() {
    return (
        <>
            <div className="container">
                <h2 className="font-semibold mt-5 text-left">User Infromation</h2>
                <table className="table-auto py-8 px-8 mt-4 w-full">
                    <tbody>
                        <tr>
                            <td className="text-left text-gray-400">Nama Lengkap</td>
                            <td className="text-right" placeholder="Nama Lengkap">John Doe</td>
                        </tr>
                        <tr>
                            <td className="text-left text-gray-400">Email</td>
                            <td className="text-right" placeholder="Email">johndoe@email.com</td>
                        </tr>
                        <tr>
                            <td className="text-left text-gray-400">Alamat</td>
                            <td className="text-right" placeholder="Alamat">Jalan Jalan</td>
                        </tr>
                        <tr>
                            <td className="text-left text-gray-400">Telepon</td>
                            <td className="text-right" placeholder="Telepon">08123456789</td>
                        </tr>
                        <tr>
                            <td className="text-left text-gray-400">Foto</td>
                            <td className="text-right" placeholder="Foto"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </>
    )
}

export default UserInfo