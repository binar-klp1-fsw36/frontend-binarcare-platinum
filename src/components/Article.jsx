function Article() {
  return (
    <>
      <div className="bg-white pt-14 pb-20">
        <div className="container mx-auto px-4 max-w-screen-xl">
          <div className="flex items-center justify-between">
            <div className="">
              <p className="mb-3 font-bold text-2xl text-blue-950">Health Articles</p>
              <p className="text-base text-gray-500">Increase insight into health</p>
            </div>
            <a class="inline-block rounded-full border border-blue-600 bg-blue-600 px-10 py-3 text-sm font-medium text-white hover:bg-transparent hover:text-blue-600 focus:outline-none focus:ring active:text-blue-500" href="/articles">
              See all article
            </a>
          </div>

          <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-2 xl:grid-cols-3 gap-6 bg-white mt-9">
            <div class="relative flex w-[400px] max-w-[400px] h-[200px] max-h-[200px] flex-row rounded-xl bg-white bg-clip-border text-gray-700 shadow-md">
              <div class="relative w-2/5 m-0 overflow-hidden text-gray-700 bg-white rounded-r-none shrink-0 rounded-xl bg-clip-border">
                <img src="https://res.cloudinary.com/dk0z4ums3/image/upload/v1677640520/attached_image/sehat-dan-lezat-buah-untuk-penyakit-jantung-ini-patut-dicoba-0-alodokter.jpg" alt="imageArticle1" class="object-cover w-full h-full" />
              </div>
              <div class="px-5 pt-3">
                <p className="mb-2 font-medium text-xs antialiased leading-relaxed tracking-normal text-gray-500">Healthy Life Article</p>
                <p class="line-clamp-2 mb-2 text-base antialiased font-semibold leading-snug tracking-normal text-blue-950">8 Buah untuk Penyakit Jantung yang Patut Dicoba</p>
                <p class="line-clamp-3 mb-2 text-xs antialiased font-normal leading-relaxed text-gray-700">Ada banyak jenis buah untuk penyakit jantung yang baik untuk dikonsumsi oleh para penderita gangguan jantung.</p>

                <a class="inline-block" href="/">
                  <button
                    class="flex items-center gap-2 pr-4 pl-1 py-3 text-xs font-bold text-center text-blue-500 uppercase align-middle transition-all rounded-lg select-none hover:bg-blue-500/10 active:bg-blue-500/30 disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none"
                    type="button"
                  >
                    Learn More
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" aria-hidden="true" class="w-4 h-4">
                      <path stroke-linecap="round" stroke-linejoin="round" d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3"></path>
                    </svg>
                  </button>
                </a>
              </div>
            </div>

            <div class="relative flex w-[400px] max-w-[400px] h-[200px] max-h-[200px] flex-row rounded-xl bg-white bg-clip-border text-gray-700 shadow-md">
              <div class="relative w-2/5 m-0 overflow-hidden text-gray-700 bg-white rounded-r-none shrink-0 rounded-xl bg-clip-border">
                <img src="https://res.cloudinary.com/dk0z4ums3/image/upload/v1677640520/attached_image/sehat-dan-lezat-buah-untuk-penyakit-jantung-ini-patut-dicoba-0-alodokter.jpg" alt="imageArticle2" class="object-cover w-full h-full" />
              </div>
              <div class="px-5 pt-3">
                <p className="mb-2 font-medium text-xs antialiased leading-relaxed tracking-normal text-gray-500">Healthy Life Article</p>
                <p class="line-clamp-2 mb-2 text-base antialiased font-semibold leading-snug tracking-normal text-blue-950">8 Buah untuk Penyakit Jantung yang Patut Dicoba</p>
                <p class="line-clamp-3 mb-2 text-xs antialiased font-normal leading-relaxed text-gray-700">Ada banyak jenis buah untuk penyakit jantung yang baik untuk dikonsumsi oleh para penderita gangguan jantung.</p>

                <a class="inline-block" href="/">
                  <button
                    class="flex items-center gap-2 pr-4 pl-1 py-3 text-xs font-bold text-center text-blue-500 uppercase align-middle transition-all rounded-lg select-none hover:bg-blue-500/10 active:bg-blue-500/30 disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none"
                    type="button"
                  >
                    Learn More
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" aria-hidden="true" class="w-4 h-4">
                      <path stroke-linecap="round" stroke-linejoin="round" d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3"></path>
                    </svg>
                  </button>
                </a>
              </div>
            </div>

            <div class="relative flex w-[400px] max-w-[400px] h-[200px] max-h-[200px] flex-row rounded-xl bg-white bg-clip-border text-gray-700 shadow-md">
              <div class="relative w-2/5 m-0 overflow-hidden text-gray-700 bg-white rounded-r-none shrink-0 rounded-xl bg-clip-border">
                <img src="https://res.cloudinary.com/dk0z4ums3/image/upload/v1677640520/attached_image/sehat-dan-lezat-buah-untuk-penyakit-jantung-ini-patut-dicoba-0-alodokter.jpg" alt="imageArtcile3" class="object-cover w-full h-full" />
              </div>
              <div class="px-5 pt-3">
                <p className="mb-2 font-medium text-xs antialiased leading-relaxed tracking-normal text-gray-500">Healthy Life Article</p>
                <p class="line-clamp-2 mb-2 text-base antialiased font-semibold leading-snug tracking-normal text-blue-950">8 Buah untuk Penyakit Jantung yang Patut Dicoba</p>
                <p class="line-clamp-3 mb-2 text-xs antialiased font-normal leading-relaxed text-gray-700">Ada banyak jenis buah untuk penyakit jantung yang baik untuk dikonsumsi oleh para penderita gangguan jantung.</p>

                <a class="inline-block" href="/">
                  <button
                    class="flex items-center gap-2 pr-4 pl-1 py-3 text-xs font-bold text-center text-blue-500 uppercase align-middle transition-all rounded-lg select-none hover:bg-blue-500/10 active:bg-blue-500/30 disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none"
                    type="button"
                  >
                    Learn More
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" aria-hidden="true" class="w-4 h-4">
                      <path stroke-linecap="round" stroke-linejoin="round" d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3"></path>
                    </svg>
                  </button>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Article;
