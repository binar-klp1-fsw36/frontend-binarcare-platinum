import React from 'react';
import Visa from '../../pictures/visa-logo.png'
import Mastercard from '../../pictures/mastercard-logo.png'
import Bitcoin from "../../pictures/bitcoin.png"
import Paypal from "../../pictures/paypal.png"

function PaymentMethod() {

    return (
        <>
            <div className="payment method max-w-sm py-8 pl-[100px] bg-white">
                <div className="text-blue-950 text-3xl text-left font-semibold">
                    Choose Your
                    <br />
                    Payment Method
                </div>
                <div className='flex justify-start gap-5 mt-4 py-4'>
                    <div className='border rounded-xl px-5'>
                        <img className='h-24 w-34' src={Visa} alt=''></img>
                    </div>
                    <div className='border rounded-xl px-5'>
                        <img className='h-24 w-34' src={Mastercard} alt=''></img>
                    </div>
                </div>
                <div className='flex justify-start gap-5 py-2'>
                    <div className='border rounded-xl px-5'>
                        <img className='h-24 w-34' src={Paypal} alt=''></img>
                    </div>
                    <div className='border rounded-xl px-5'>
                        <img className='h-24 w-34' src={Bitcoin} alt=''></img>
                    </div>
                </div>
            </div>
        </>
    )
}

export default PaymentMethod