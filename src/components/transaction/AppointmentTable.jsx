import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';

function AppointmentTable({ onAppointmentData }) {
    const location = useLocation();
    const { formData } = location.state || {};
    const [data, setData] = useState(null);

    useEffect(() => {
        if (formData) {
            setData(formData);
            onAppointmentData(formData);
        }
    }, [formData, onAppointmentData]);

    return (
        <>
            <div className="container">
                <h2 className="font-semibold mt-5 text-left">Appointment</h2>
                <table className="table-auto py-8 px-8 mt-4 w-full">
                    <tbody>
                        <tr>
                            <td className="text-left text-gray-400">Kebutuhan konsultasi</td>
                            <td className="text-right">{data?.topic || 'N/A'}</td>
                        </tr>
                        <tr>
                            <td className="text-left text-gray-400">Level</td>
                            <td className="text-right">{data?.level || 'N/A'}</td>
                        </tr>
                        <tr>
                            <td className="text-left text-gray-400">Dijadwalkan pada</td>
                            <td className="text-right">{data?.date || 'N/A'}</td>
                        </tr>
                        <tr>
                            <td className="text-left text-gray-400">Waktu</td>
                            <td className="text-right">{data?.time || 'N/A'}</td>
                        </tr>
                        <tr>
                            <td className="text-left text-gray-400">Status</td>
                            <td className="text-right">Waiting for payment</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </>
    )
}

export default AppointmentTable