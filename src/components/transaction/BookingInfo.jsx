// BookingInfo.js
import jsPDF from 'jspdf';
import React from 'react';

function generateInvoice(appointmentData) {
    const doc = new jsPDF();
  
    doc.setFontSize(14);
    doc.text('Invoice', 10, 10);
  
    const startY = 20;
    const lineSpacing = 10;
    doc.text(`Kebutuhan konsultasi: ${appointmentData?.topic || 'N/A'}`, 10, startY + lineSpacing * 1);
    doc.text(`Dijadwalkan pada: ${appointmentData?.date || 'N/A'}`, 10, startY + lineSpacing * 2);
    doc.text(`Waktu: ${appointmentData?.time || 'N/A'}`, 10, startY + lineSpacing * 3);
    doc.text('Grand Total:', 10, startY + lineSpacing * 4);
    doc.text('Status: Pending', 10, startY + lineSpacing * 5);
  
    return doc;
  }
  
  function downloadInvoice() {
    const appointmentData = {};
  
    const doc = generateInvoice(appointmentData);
  
    doc.save('invoice.pdf');
  }

function BookingInfo({ appointmentData }) {
  return (
    <>
      <div className="container">
        <h2 className="font-semibold mt-5 text-left">Booking Information</h2>
        <table className="table-auto py-8 px-8 mt-4 w-full">
          <tbody>
            <tr>
              <td className="text-left text-gray-400">Kebutuhan konsultasi</td>
              <td className="text-right">{appointmentData?.topic || 'N/A'}</td>
            </tr>
            <tr>
              <td className="text-left text-gray-400">Dijadwalkan pada</td>
              <td className="text-right">{appointmentData?.date || 'N/A'}</td>
            </tr>
            <tr>
              <td className="text-left text-gray-400">Waktu</td>
              <td className="text-right">{appointmentData?.time || 'N/A'}</td>
            </tr>
            <tr>
              <td className="text-left text-gray-400">Grand Total</td>
              <td className="text-right"></td>
            </tr>
            <tr>
              <td className="text-left text-gray-400">Status</td>
              <td className="text-right">Pending</td>
            </tr>
          </tbody>
        </table>
        <button className='mt-5 w-full bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full' 
        onClick={downloadInvoice}>
        Download Invoice
        </button>
      </div>
    </>
  );
}

export default BookingInfo;
