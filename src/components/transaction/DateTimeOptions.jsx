import React from "react";

function DateTimeOptions({ date, time, onDateChange, onTimeChange }) {

    const generateDateOptions = () => {
        const today = new Date();
        const dateOptions = [];

        for (let i = 0; i < 30; i++) {
            const date = new Date(today);
            date.setDate(today.getDate() + i);

            if (date < today) {
                dateOptions.push(
                    <option key={i} value={date.toLocaleDateString()} disabled>
                        {date.toDateString()} (Past)
                    </option>
                );
            } else {
                dateOptions.push(
                    <option key={i} value={date.toLocaleDateString()}>
                        {date.toDateString()}
                    </option>
                );
            }
        }

        return dateOptions;
    };

    const generateTimeOptions = () => {
        const timeOptions = [];
        const now = new Date();
        const startHour = 8;
        const endHour = 17;
        const interval = 30;
        
        for (let hour = startHour; hour < endHour; hour++) {
            for (let minute = 0; minute < 60; minute += interval) {
                const time = new Date(now);
                time.setHours(hour, minute, 0);

                if (time >= now && hour >= startHour && hour < endHour) {
                  timeOptions.push(
                      <option key={time.toISOString()} value={time.toLocaleTimeString()}>
                          {time.toLocaleTimeString()}
                      </option>
                  );
              }              
            }
        }
        return timeOptions;
    };
    return (
        <>
          <label className="block">
            <select
              className="block w-full mt-4 rounded-3xl border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
              value={date}
              onChange={(e) => onDateChange(e.target.value)}
            >
              {generateDateOptions()}
            </select>
          </label>
          <label className="block">
            <select
              className="block w-full mt-4 rounded-3xl border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
              value={time}
              onChange={(e) => onTimeChange(e.target.value)}
            >
              {generateTimeOptions()}
            </select>
          </label>
        </>
      );
}

export default DateTimeOptions