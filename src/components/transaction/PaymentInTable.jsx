
function PaymentTable() {
    const biayaKonstultasi = 500000;
    const feeDokter = 20000;
    const feeHospital = 10000;

    const ppn = 0.1 * (biayaKonstultasi + feeDokter + feeHospital);
    const grandTotal = biayaKonstultasi + feeDokter + feeHospital + ppn;

    return (
        <>
            <div className="container">
                <h2 className='font-semibold mt-5 text-left'>Payment Information</h2>
                <table className="table-auto py-8 px-8 mt-4 w-full">
                    <tbody>
                        <tr>
                            <td className='text-left text-gray-400'>Biaya konstultasi</td>
                            <td className='text-right'>Rp. {biayaKonstultasi}</td>
                        </tr>
                        <tr>
                            <td className='text-left text-gray-400'>Fee dokter</td>
                            <td className='text-right'>Rp. {feeDokter}</td>
                        </tr>
                        <tr>
                            <td className='text-left text-gray-400'>Fee hospital</td>
                            <td className='text-right'>Rp. {feeHospital}</td>
                        </tr>
                        <tr>
                            <td className='text-left text-gray-400'>Ppn 10%</td>
                            <td className='text-right'>Rp. {ppn}</td>
                        </tr>
                        <tr>
                            <td className='text-left text-gray-400'>Grand total</td>
                            <td className='text-right text-green-500'>Rp. {grandTotal}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default PaymentTable;
