import React, { useState } from 'react'
import DateTimeOptions from './DateTimeOptions';

function AppointmentForm({ navigate }) {

    const [topic, setTopic] = useState('');
    const [level, setLevel] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
  
    
    const handleSubmit = () => {
        const formData = {
        topic,
        level,
        date,
        time,
        };

        navigate('/checkout', { state: { formData } });

    };
    return (
        <>
          <div className="appointment form max-w-xl py-8 pl-[100px] bg-white">
          <div className="text-blue-950 text-3xl text-left font-semibold">
            Apply for
            <br />
            New Appointment
          </div>

          <div className="py-4 mt-4">
            <label className="block">
              <select
                className="block w-full mt-2 rounded-3xl border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                value={topic}
                onChange={(e) => setTopic(e.target.value)}
              >
                <option value="" disabled>Topik Konsultasi</option>
                <option value="Penyakit Dalam">Penyakit Dalam</option>
                <option value="Jantung">Jantung</option>
                <option value="Saraf">Saraf</option>
              </select>
            </label>
            <label className="block">
              <select
                className="block w-full mt-4 rounded-3xl border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                value={level}
                onChange={(e) => setLevel(e.target.value)}
              >
                <option value="" disabled>Level</option>
                <option value="High">High</option>
                <option value="Medium">Medium</option>
                <option value="Low">Low</option>
              </select>
            </label>
            <div className="block">
              <DateTimeOptions
                date={date}
                time={time}
                onDateChange={setDate}
                onTimeChange={setTime}
              />
            </div>
            <button
              className="mt-10 w-full bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
              onClick={handleSubmit}
            >
              Continue
            </button>
          </div>
        </div>
        </>
    )
}

export default AppointmentForm