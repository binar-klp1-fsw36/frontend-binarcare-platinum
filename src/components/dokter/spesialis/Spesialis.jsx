import axios from "axios"
import { useEffect, useState } from "react"

const Spesialis = () => {
    const [spesialiss,setSpesialis] = useState([])

    useEffect(() => {
        const loadData = async () => {
            try{
                const response2 = await axios.get('https://binarcare-api.vercel.app/api/v1/spesialis')
                
                setSpesialis(response2.data.data)
            }
            catch(err)
            {
                console.log(err)
            }
        }
        loadData()
    },[])

    return <>
        <div style={{
            width:300,
            marginLeft:60,
            marginTop:20
        }} class="block p-6 bg-white border border-gray-200 rounded-lg shadow">
            <input defaultChecked id="default-radio-1" type="radio" value="" name="default-radio" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"/>
            <label for="default-radio-1" class="ml-2 text-sm font-medium ">All</label>
            <br />
            {spesialiss.map((spesial) => (
                <div key={spesial.id_spesialis}>
                    <input id="default-radio-2" type="radio" value="" name="default-radio" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"/>
                    <label for="default-radio-2" class="ml-2 text-sm font-medium ">{spesial.nm_spesialis}</label>
                    <br />
                </div>

            ))}
        </div>
    </>
}


export default Spesialis