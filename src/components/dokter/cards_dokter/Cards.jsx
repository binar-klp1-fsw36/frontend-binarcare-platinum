import '../cards_dokter/index.css'
import star from '../cards_dokter/star.svg'
import fotodokter from '../cards_dokter/dokter.svg'
import axios from "axios"
import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';

function getNamaSpesialis(id) {
    var nama = ''
    if(id === 'SPS1') nama = 'Penyakit dalam'
    else if(id === 'SPS2') nama = 'Ortopedi'
    else if(id === 'SPS3') nama = 'Paru - paru'
    else if(id === 'SPS4') nama = 'Gigi'
    else if(id === 'SPS5') nama = 'Anak'
    else if(id === 'SPS6') nama = 'Bedah tulang'
    return nama
}

function Cards() {

    const [dokters,setDokters] = useState([])
    const [id, setId] = useState('')
    const navigate = useNavigate();
    
    useEffect(() => {
        const loadData = async () => {
            try{
                const response = await axios.get('https://binarcare-api.vercel.app/api/v1/dokters')
                setDokters(response.data.data)
            }
            catch(err){
                console.log(err)
            }
        }
        loadData()
    },[])

    const handleSubmit = (id_dokter) => {
        const formData = {
        id_dokter,
        };

        navigate('/checkout', { state: { formData } });
    };

    return <>

        <ul>
        {dokters.map((dokter) => (
        <div key={dokter.id_dokter} className="box">
            <div className="group">
                <div className="div">
                    <img className="img" alt="Group" src={fotodokter} />
                    <div className="group-2">
                        <div className="group-3">
                            <p className="p">{dokter.nama_dokter}</p>
                            <div className="text-wrapper-2">{getNamaSpesialis(dokter.id_spesialis)}</div>
                            <div className="text-wrapper">{dokter.alamat_dokter}</div>
                        </div>
                        <div className="group-4">
                            <img className="star" alt="Star" src={star} />
                            <img className="star-2" alt="Star" src={star} />
                            <img className="star-3" alt="Star" src={star} />
                            <img className="star-4" alt="Star" src={star} />
                            <img className="star-5" alt="Star" src={star} />
                            <div className="text-wrapper-3">(126)
                            </div>
                        </div>
                    </div>
                </div>
                <div className="group-5">
                    <div className="text-wrapper-4">Biaya Konsultasi</div>
                    <div className="text-wrapper-5">Rp {dokter.tarif_dokter.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</div>
                </div>
                <div className="overlap-group-wrapper">
                <div>
                    <div >
                    <button onClick={(e) => handleSubmit(dokter.id_dokter)} style={{fontSize:20}} type="button" class="overlap-group text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Buat Janji</button>
                    </div>
                </div>
                </div>
            </div>
        </div>
        ))}
        </ul>

    </>
}

export default Cards