import './MiniCard.css'

function MiniCard() {
  return (
    <div className="card-container">
      <div className="img-container">
        <img className='mini-img' src="https://images.unsplash.com/photo-1576091160550-2173dba999ef?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80"></img>
      </div>
      <div className="caption-container">
        <p id='title-rec'>Judul</p>
        <p id='caption'>Ini caption untuk card</p>
      </div>
    </div>
  );
}

export default MiniCard;
