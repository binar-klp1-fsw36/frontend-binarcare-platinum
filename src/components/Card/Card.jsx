import "./Card.css";

function Card(props) {
  return (
    <a href={props.redirect} className="card">
        <div className="picture-container">
          <img alt={props.alt} src={props.src} className="card-thumbnail" />
        </div>
        <div className="text-container">
          <p>{props.text}</p>
        </div>
    </a>
  );
}

export default Card;
