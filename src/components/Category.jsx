import iconDoctor from '../assets/icon-doctor.svg';
import iconSpecialist from '../assets/icon-specialistMedical.svg';
import iconArticle from '../assets/icon-articles.svg';
import iconDrug from '../assets/icon-drug.svg';

function Category() {
  return (
    <>
      <div className="bg-slate-50 pt-36 pb-10">
        <div className="container mx-auto px-4 max-w-screen-xl">
          <p className="mb-3 font-bold text-2xl text-blue-950">Our Main Services Categories</p>
          <p className="text-base text-gray-500">Quick way to get your first experience</p>

          <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-6 bg-slate-50 mt-10">
            <div class="w-[280px] h-[150px] pt-2 md:pt-4 text-center rounded-xl bg-white shadow-lg">
              <div class="inline-flex flex items-center justify-center rounded-full w-14 h-14 border border-blue-700 bg-white">
                <div className="w-12 h-12 rounded-full flex flex items-center justify-center bg-white shadow-lg p-3">
                  <img src={iconDoctor} className="w-8 h-8" alt="icon-doctor" />
                </div>
              </div>

              <h3 class="mt-2 text-2xl md:text-2xl font-semibold text-blue-950">Doctors</h3>
              <p class=" text-base md:text-base text-gray-400">147 Doctors</p>
            </div>

            <div class="w-[280px] h-[150px] pt-2 md:pt-4 text-center rounded-xl bg-white shadow-lg">
              <div class="inline-flex flex items-center justify-center rounded-full w-14 h-14 border border-blue-700 bg-white">
                <div className="w-12 h-12 rounded-full flex flex items-center justify-center bg-white shadow-lg p-3">
                  <img src={iconSpecialist} className="w-8 h-8" alt="icon-specialist-medical" />
                </div>
              </div>

              <h3 class="mt-2 text-2xl md:text-2xl font-semibold text-blue-950">Medical Specialist</h3>
              <p class=" text-base md:text-base text-gray-400">17 Specialist</p>
            </div>

            <div class="w-[280px] h-[150px] pt-2 md:pt-4 text-center rounded-xl bg-white shadow-lg">
              <div class="inline-flex flex items-center justify-center rounded-full w-14 h-14 border border-blue-700 bg-white">
                <div className="w-12 h-12 rounded-full flex flex items-center justify-center bg-white shadow-lg p-3">
                  <img src={iconArticle} className="w-8 h-8" alt="icon-articles" />
                </div>
              </div>

              <h3 class="mt-2 text-2xl md:text-2xl font-semibold text-blue-950">Health Articles</h3>
              <p class=" text-base md:text-base text-gray-400">23 Articles</p>
            </div>

            <div class="w-[280px] h-[150px] pt-2 md:pt-4 text-center rounded-xl bg-white shadow-lg">
              <div class="inline-flex flex items-center justify-center rounded-full w-14 h-14 border border-blue-700 bg-white">
                <div className="w-12 h-12 rounded-full flex flex items-center justify-center bg-white shadow-lg p-3">
                  <img src={iconDrug} className="w-8 h-8" alt="icon-drug-medicine" />
                </div>
              </div>

              <h3 class="mt-2 text-2xl md:text-2xl font-semibold text-blue-950">Medicine</h3>
              <p class="text-base md:text-base text-gray-400">1.675 Medicine</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Category;
