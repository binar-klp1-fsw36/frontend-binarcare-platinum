import React from 'react';
import '../App.css';
import { SerachDoctor } from './SearchDoctor';
import ImgHeroDoctor1 from '../assets/ImgHeroDoctor1.svg';
import ImgHeroDoctor2 from '../assets/ImgHeroDoctor2.svg';

function Hero() {
  return (
    <>
      <div className="bg-blue-100 h-[520px] relative">
        <div className=" container w-full max-w-screen-xl mx-auto px-4 mb-12">
          <div className=" sm:grid sm:grid-cols-2 flex mb-14">
            <div className="mt-14">
              <p className="mb-5 font-bold text-6xl text-blue-950">Binar Care Ready to</p>
              <p className="mb-5 font-bold text-6xl from-blue-400 to-blue-800 bg-gradient-to-r bg-clip-text text-transparent">Help Your Health</p>
              <p className="mb-5 font-bold text-6xl text-blue-950">Problems</p>
              <p className="font-normal text-lg text-black w-[550px]">
                In times like today, your health is very important, especially since the number of COVID-19 cases is increasing day by day, so we are ready to help you with your health consultation
              </p>
            </div>
            <div className="flex justify-end">
              <div className="absolute hidden mr-40 md:block h-[580px] w-[400px]">
                <img class="object-cover mt-10 h-full w-full sm:h-[calc(100%_-_2rem)] sm:self-end sm:rounded-[35px] md:h-[calc(100%_-_4rem)] md:rounded-[35px]" src={ImgHeroDoctor1} alt="imageDoctor" />
              </div>
              <div className="absolute -mr-12 hidden lg:block mt-10 object-cover h-[500px] w-[175px] ">
                <img class="ml-auto mt-14 sm:h-[calc(100%_-_2rem)] sm:self-end sm:rounded-ss-[35px] md:h-[calc(100%_-_4rem)] md:rounded-ss-[35px]" src={ImgHeroDoctor2} alt="imageDoctor" />
              </div>
            </div>
          </div>
          <SerachDoctor />
        </div>
      </div>
    </>
  );
}

export default Hero;
