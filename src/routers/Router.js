import {BrowserRouter,Route,Routes} from "react-router-dom"
import Dokterlist from "../pages/list_dokter/Dokterlist"
import LandingPage from '../pages/landing_page/Landing_Page'
import ArticleList from "../pages/ArticleList/ArticleList"
import ArticlePage from "../pages/ArticlePage/ArticlePage"
import MiniCard from "../components/MiniCard/MiniCard"
import "swagger-ui-react/swagger-ui.css";
import SwaggerUI from "swagger-ui-react";
import swaggerData from "../openapi.json"
import Transaction from "../pages/transaction/Transaction"
import SuccesCheckout from "../pages/transaction/SuccessCheckout"
import Checkout from "../pages/transaction/Checkout"
import UserDashboard from "../pages/userDashboard/UserDashboard"

function Router() {
  return <>
  <BrowserRouter>
      <Routes>
        <Route path="/" element={<LandingPage />}></Route>
        <Route path="/dokter" element={<Dokterlist/>}></Route>
        <Route path="/docs" element={<SwaggerUI spec={swaggerData}/>}/>
        <Route path="/transaction" element={<Transaction/>}></Route>
        <Route path="/checkout" element={<Checkout/>}></Route>
        <Route path="/checkout/success" element={<SuccesCheckout/>}></Route>
        <Route path="/user-dashboard" element={<UserDashboard/>}></Route>
        <Route path="/test" element={<MiniCard />}></Route>
        <Route path="/articles" element={<ArticleList />}></Route>
        <Route path="/read" element={<ArticlePage />}></Route>
      </Routes>
    </BrowserRouter>
  </>
}
export default Router
