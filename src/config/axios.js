import axios from 'axios';

const api = axios.create({
  baseURL: 'https://binarcare-api.vercel.app',
});

export default api;