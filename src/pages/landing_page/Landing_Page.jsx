import Navbar from '../../components/Navbar';
import Hero from '../../components/Hero';
import Category from '../../components/Category';
import BestDoctor from '../../components/BestDoctor';
import Article from '../../components/Article';
import Footer from '../../components/Footer';

function LandingPage() {
  return (
    <>
      <Navbar />
      <Hero />
      <Category />
      <BestDoctor />
      <Article />
      <Footer />
    </>
  );
}

export default LandingPage;
