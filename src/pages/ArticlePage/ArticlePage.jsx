import "./Article.css";
import MiniCard from "../../components/MiniCard/MiniCard";
import Header from "../../components/Navbar";
import Footer from "../../components/Footer";

function Article_Page() {
  return (
    <>
      <Header />
      <div className="grid grid-row-2 grid-cols-10 min-h-screen w-full h-full overflow-clip">
        <div className="py-8 pt-20 col-start-2 col-span-5 ">
          <h1 className="font-bold text-lg pb-5">Menjaga pola hidup sehat</h1>
          <p className="italic pb-5">
            last updated: 26/10/2023 <br />
            by binarcare
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam egestas
            magna et dapibus pulvinar. Proin et dolor luctus elit semper
            feugiat. Nullam vitae pulvinar turpis. Curabitur vestibulum pulvinar
            cursus. Nam est nulla, mattis et pulvinar sed, ultrices eu lorem.
            Sed pharetra commodo est eu lobortis. Donec rutrum ac quam sodales
            vestibulum. Fusce dictum, mi vitae dapibus placerat, dui leo
            tincidunt nibh, at fringilla sem turpis vel tellus. Integer sed
            risus leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Nam egestas magna et dapibus pulvinar. <br></br>Proin et dolor
            luctus elit semper feugiat. Nullam vitae pulvinar turpis. Curabitur
            vestibulum pulvinar cursus. Nam est nulla, mattis et pulvinar sed,
            ultrices eu lorem. Sed pharetra commodo est eu lobortis. Donec
            rutrum ac quam sodales vestibulum. Fusce dictum, mi vitae dapibus
            placerat, dui leo tincidunt nibh, at fringilla sem turpis vel
            tellus. Integer sed risus leo.<br></br>Proin et dolor luctus elit
            semper feugiat. Nullam vitae pulvinar turpis. Curabitur vestibulum
            pulvinar cursus. Nam est nulla, mattis et pulvinar sed, ultrices eu
            lorem. Sed pharetra commodo est eu lobortis. Donec rutrum ac quam
            sodales vestibulum. Fusce dictum, mi vitae dapibus placerat, dui leo
            tincidunt nibh, at fringilla sem turpis vel tellus. Integer sed
            risus leo.<br></br>Proin et dolor luctus elit semper feugiat. Nullam
            vitae pulvinar turpis. Curabitur vestibulum pulvinar cursus. Nam est
            nulla, mattis et pulvinar sed, ultrices eu lorem. Sed pharetra
            commodo est eu lobortis. Donec rutrum ac quam sodales vestibulum.
            Fusce dictum, mi vitae dapibus placerat, dui leo tincidunt nibh, at
            fringilla sem turpis vel tellus. Integer sed risus leo.<br></br>
            Proin et dolor luctus elit semper feugiat. Nullam vitae pulvinar
            turpis. Curabitur vestibulum pulvinar cursus. Nam est nulla, mattis
            et pulvinar sed, ultrices eu lorem. Sed pharetra commodo est eu
            lobortis. Donec rutrum ac quam sodales vestibulum. Fusce dictum, mi
            vitae dapibus placerat, dui leo tincidunt nibh, at fringilla sem
            turpis vel tellus. Integer sed risus leo.
          </p>
        </div>
        <div className="rec-container col-start-8 py-8 pt-20 col-span-2">
          <h1 className="font-bold text-lg pb-5">Recommended</h1>
          <MiniCard />
        </div>
      </div>
      <Footer />
    </>
  );
}

export default Article_Page;
