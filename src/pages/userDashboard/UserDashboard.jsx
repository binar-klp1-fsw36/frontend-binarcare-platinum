import { useLocation } from 'react-router-dom';
import SelectedUser from "../../components/userDashboard/SelectedUser"
import UserInfo from "../../components/userDashboard/UserInfo"
import Navbar from '../../components/Navbar'
import Footer from '../../components/Footer'
import BookingInfo from "../../components/transaction/BookingInfo"

function UserDashboard() {
    const location = useLocation();
    const { appointmentData } = location.state || {};
    console.log('appointmentData:', appointmentData);
    return(
        <>
            <Navbar />
            <div className="container max-w-lg mx-auto mt-12 mb-12">
                <SelectedUser />
                <UserInfo />
                <BookingInfo appointmentData={appointmentData} />
            </div>
            <Footer />
        </>
    )
}

export default UserDashboard