import SelectedDoctor from '../../components/transaction/SelectedDoctor';
import AppointmentForm from '../../components/transaction/ApointmentForm.jsx';
import Navbar from '../../components/Navbar'
import Footer from '../../components/Footer'
import { useNavigate } from 'react-router-dom';

function Transaction() {
  const navigate = useNavigate(); 

  return (
    <>
      <Navbar />
      <div className="container mx-auto grid grid-cols-2 mt-12 items-center divide-x">
        
        <SelectedDoctor />
        
        <AppointmentForm navigate={navigate} /> 

      </div>
      <Footer />
    </>
  );
}

export default Transaction;
