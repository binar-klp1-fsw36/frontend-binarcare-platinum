import React, { useState } from 'react';
import ChosenDoctor from '../../components/transaction/chosenDoctor';
import AppointmentTable from '../../components/transaction/AppointmentTable';
import PaymentTable from '../../components/transaction/PaymentInTable';
import PaymentMethod from '../../components/transaction/PaymentMethod';
import Navbar from '../../components/Navbar'
import Footer from '../../components/Footer'
import { useNavigate, useLocation } from 'react-router-dom';

function Checkout() {
    const location = useLocation();
    const navigate = useNavigate();

    const [appointmentData, setAppointmentData] = useState(null);

    const handleAppointmentData = (data) => {
        console.log('Data received in handleAppointmentData:', data);
        setAppointmentData(data);
    };
    console.log('appointmentData', appointmentData)
    const handleCheckout = () => {
        console.log('Data before navigation:', appointmentData);
        navigate('/checkout/success', { state: appointmentData });
    }

    console.log('appointmentData', appointmentData)

    return (
        <>
            <Navbar />
            <div className='container mx-auto text-center mb-12'>
            <div className="container grid grid-cols-2 mx-auto divide-x mt-12">
                <div className='mx-auto'>
                    <ChosenDoctor />
                    <AppointmentTable onAppointmentData={handleAppointmentData} />
                    <PaymentTable />
                </div>
                <PaymentMethod />
                
            </div>
            <button className="mt-5 bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
                    onClick={handleCheckout}>
                    Book Now
            </button>
            </div>
            <Footer />
        </>
    )
}

export default Checkout