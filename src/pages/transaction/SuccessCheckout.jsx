import success from '../../pictures/success.png'
import { useNavigate, useLocation } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import Navbar from '../../components/Navbar'
import Footer from '../../components/Footer'

function SuccesCheckout() {
    const navigate = useNavigate();
    const location = useLocation();
    const [appointmentData, setAppointmentData] = useState({});

    useEffect(() => {
        const { appointmentData: data } = location.state || {};
        setAppointmentData(data);
    }, [location.state]);

    const handleSubmit = () => {
        if (appointmentData !== null) {
            navigate('/user-dashboard', { appointmentData });
        } else {
            console.log('Error: appointmentData is null.');
        }
    }

    console.log('SuccessCheckout-appointmentData:', appointmentData);
    return (
        <>
            <Navbar />
            <div className="container mx-auto mt-12">
                <div className="text-center py-8 bg-white">
                    <img className="block mx-auto h-24 rounded-full" src={success} alt="Success" />
                    <div className="text-center mt-10">
                        <p className="text-4xl text-black font-semibold">
                            Your booking has been placed!
                        </p>
                        <p className="text-slate-500 font-medium mt-4">
                            Pastikan anda tidak terlambat untuk mendapatkan <br />
                            konstultasi yang terbaik
                        </p>
                    </div>
                    <button className="mt-10 bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
                    onClick={handleSubmit}>
                        My Dashboard
                    </button>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default SuccesCheckout