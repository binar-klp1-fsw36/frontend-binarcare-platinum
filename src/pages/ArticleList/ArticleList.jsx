import Column from "../../components/CardColumn/Column";
import ArticleHero from "../../components/ArticleHero/ArticleHero";
import Header from "../../components/Navbar"
import Footer from "../../components/Footer"

function Landing_Page() {
  return (
    <>
      <Header />
      <ArticleHero />
      <Column />
      <Footer />
    </>
  );
}

export default Landing_Page;
