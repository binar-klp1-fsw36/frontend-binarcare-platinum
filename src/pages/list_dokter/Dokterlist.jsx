import Cards from '../../components/dokter/cards_dokter/Cards'
import {Dokter} from '../../components/dokter/list_dokter/Dokter'
import Spesialis from '../../components/dokter/spesialis/Spesialis'
import Navbar from '../../components/Navbar'
import Footer from '../../components/Footer'

const Dokterlist = () => {
    return <>
    <Navbar/>
        <div className="mx-auto grid grid-cols-1 items-center divide-x">
            <div className=''>
                <Dokter />
            </div>
            <div className='grid grid-cols-3 '>
                <div className='items-center justify-center'>
                    <Spesialis />
                </div>
                <div className='items-center justify-center'>
                    <Cards />
                </div>

            </div>
        </div>
    <Footer/>
    </>
}


export default Dokterlist